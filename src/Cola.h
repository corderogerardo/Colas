/*
 * Cola.h
 *
 *  Created on: 25/5/2015
 *      Author: Gerardo Cordero_2
 */

#ifndef COLA_H_
#define COLA_H_
#include <fstream>
#include <iostream>
#include <string>
#include <stdlib.h>

#include "Node.cpp"

using namespace std;

template <class T>

class Cola {
private:
	Node<T> *m_inicio, *m_fin;
public:
	Cola();
	~Cola();

	 void agregar(T,char);
	 int retirar();
	  int frecuencia(int pos);
	  int cuantos_hay();
	  bool colaVacia();
	  void imprimir();
	  void eliminarImpares();
	  void suprimir_mayores_a(int limite);


	 int mayor();
	 int menor();

};

#endif /* COLA_H_ */
