/*
 * Cola.cpp
 *
 *  Created on: 25/5/2015
 *      Author: Gerardo Cordero_2
 */

#include "Cola.h"
using namespace std;

template<typename T>
Cola<T>::Cola() {
	// TODO Auto-generated constructor stub
	 m_inicio = NULL;
	 m_fin = NULL;
}

// Insertar al inicio
template<typename T>
void Cola<T>::agregar(T data_, char prioridad_)
{
	Node<T> *new_node = new Node<T> (data_,prioridad_);
	    Node<T> *temp = m_fin;

	    if (!m_inicio) {
	    	m_inicio = new_node;
	    	m_fin=new_node;
	    } else {
	        while (temp->siguiente != NULL) {
	            temp = temp->siguiente;
	        }
	        new_node->siguiente = temp->siguiente;
	        new_node->anterior = temp;
	        temp->siguiente=new_node;
	        m_fin=new_node;
	    }
}

template<typename T>
int Cola<T>::retirar()
{
	Node<T> *a_eliminar = m_inicio;
	int valorAlInicio=0;
	if(cuantos_hay()>0){
		valorAlInicio=m_inicio->data;
		m_inicio=a_eliminar->siguiente;
		delete a_eliminar;
	return valorAlInicio;
	}
}

template<typename T>
void Cola<T>::eliminarImpares()
{
	Node<T> *a_eliminar = m_inicio;
	Node<T> *auxNodo = m_inicio;

	int valorAlInicio=0;
	valorAlInicio = m_inicio->data;
	int impar=0;
	impar=valorAlInicio%2;
		while((auxNodo!=NULL) && (impar>0))
		{
			m_inicio=auxNodo->siguiente;
			delete auxNodo;
		}
		while(auxNodo!=NULL)
		{
			a_eliminar=auxNodo->siguiente;
			if((a_eliminar->data%2)>0)
			{
				auxNodo->siguiente=a_eliminar->siguiente;
				delete a_eliminar;
			}
			auxNodo=auxNodo->siguiente;
		}


}

template<typename T>
int Cola<T>::frecuencia(int pos)
{
	Node<T> *auxNode = m_inicio;
	Node<T> *tempo = m_inicio;
	int frecuencia=0;

	frecuencia=0;
		while(auxNode!=NULL)
		{
			int valor=0;
				for(int i=0;i<pos;i++)
				{
					m_inicio=m_inicio->siguiente;
				}
				valor=m_inicio->data;
				m_inicio=tempo;

		if(auxNode->data==valor)
		{
			frecuencia++;
		}
		auxNode=auxNode->siguiente;
		}
		return frecuencia;
}




template<typename T>
bool Cola<T>::colaVacia(){
	 Node<T> *aux_node = m_inicio;
		int cantidad=0;
			if(m_inicio == NULL)
			{
		      return true;
			}
			else{
				return false;
			}

}

template<typename T>
int Cola<T>::cuantos_hay()
{
	 Node<T> *aux_node = m_inicio;

	int cantidad=0;

		if(m_inicio == NULL){
	   	puts("No hay elementos en la lista");
	      return 0;
	   }
	   else{
	   	while(m_inicio!=NULL){
	      	cantidad ++;
	      	m_inicio = m_inicio->siguiente;
	      }
	   	m_inicio=aux_node;
	      return(cantidad);
	   }
}

// Imprimir la cola
template<typename T>
void Cola<T>::imprimir()
{
    Node<T> *temp = m_inicio;
    if (!m_inicio) {
        cout << "La Lista est� vac�a " << endl;
    } else {
        while (temp) {
            temp->print();
            if (!temp->siguiente) cout << "NULL";
                temp = temp->siguiente;
        }
  }
  cout << endl << endl;
}

// Mayor
template<typename T>
int Cola<T>::mayor()
{
	int el_mayor;
	Node<T> *lugar = m_inicio;
	el_mayor = lugar->data;
	while(lugar->siguiente!=NULL)
	{
		if(lugar->data>el_mayor)
		{
			el_mayor=lugar->data;

		}
		lugar=lugar->siguiente;
	}
	return el_mayor;
}
// Menor
template<typename T>
int Cola<T>::menor()
{
	int el_menor;
		Node<T> *lugar = m_inicio;
		el_menor = lugar->data;
		while(lugar->siguiente!=NULL)
		{
			if(lugar->data<el_menor)
			{
				el_menor=lugar->data;

			}
			lugar=lugar->siguiente;
		}
		return el_menor;

}

//suprimir valores mayores a un limite dado
template<typename T>
void Cola<T>::suprimir_mayores_a(int limite)
{
	Node<T> *anterior = m_inicio;
	Node<T> *actual = m_inicio;
	while(actual->siguiente!=NULL)
	{
		if(actual->data>limite)
		{
			m_inicio=actual->siguiente;
			delete actual;
			actual = m_inicio;
		}
		actual=actual->siguiente;
	}

}


template<typename T>
Cola<T>::~Cola() {

}

