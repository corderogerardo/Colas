#ifndef NODE_H
#define NODE_H

#include <iostream>

using namespace std;

template <class T>

class Node
{
    public:
        Node();
        Node(T,char);
        ~Node();

        Node *siguiente;
        Node *anterior;
        T data;
        char prioridad;

       void delete_all();
       void print();
};

#endif // NODE_H
